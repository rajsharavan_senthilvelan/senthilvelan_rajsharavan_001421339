/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rajsharavan
 */
public class CLClass {

    public static void main(String args[]) {

        try {
            Product product = new Product();
            System.out.println("Create Product");
            BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Name of the Product : ");
            String productName = read.readLine();
            product.setName(productName);
            System.out.print("Price of the Product : ");
            double price;
            while (true) {
                try {
                    price = Double.parseDouble(read.readLine());
                    product.setPrice(price);
                    break;
                } catch (Exception e) {
                    System.out.print("Price of the Product : ");
                }
            }
            System.out.print("Availability Number : ");
            int avaNum;
            while (true) {
                try {
                    avaNum = Integer.parseInt(read.readLine());
                    product.setAvailNum(avaNum);
                    break;
                } catch (Exception e) {
                    System.out.print("Availability Number : ");
                }
            }
            System.out.print("Description : ");
            String desc = read.readLine();
            product.setDescription(desc);
            System.out.print("Supplier Name :");
            String supName = read.readLine();
            product.getSupplier().setSupplierName(supName);
            System.out.print("Supplier Address : ");
            String supAdd = read.readLine();
            product.getSupplier().setSupplierAdd(supAdd);
            System.out.print("Supplier Contact Number : ");
            long supNum;
            while (true) {
                try {
                    supNum = Long.parseLong(read.readLine());
                    product.getSupplier().setSupplierNum(supNum);
                    break;
                } catch (Exception e) {
                    System.out.print("Supplier Contact Number : ");
                }
            }
            System.out.println("");
            System.out.println("View Product");
            System.out.println("Name of the Product : " + product.getName());
            System.out.println("Price of the Product : " + product.getPrice());
            System.out.println("Availability Number : " + product.getAvailNum());
            System.out.println("Description : " + product.getDescription());
            System.out.println("Supplier Name :" + product.getSupplier().getSupplierName());
            System.out.println("Supplier Address : " + product.getSupplier().getSupplierAdd());
            System.out.println("Supplier Contact Number : " + product.getSupplier().getSupplierNum());

            System.out.println("");
            System.out.println("Do you want to Update the Product : Y/N");
            String result = read.readLine();

            if (result.equalsIgnoreCase("Y")) {
                System.out.print("Name of the Product : ");
                String productName1 = read.readLine();
                product.setName(productName1);
                System.out.print("Price of the Product : ");
                double price1;
                while (true) {
                    try {
                        price1 = Double.parseDouble(read.readLine());
                        product.setPrice(price1);
                        break;
                    } catch (Exception e) {
                        System.out.print("Price of the Product : ");
                    }
                }
                System.out.print("Availability Number : ");
                int avaNum1;
                while (true) {
                    try {
                        avaNum1 = Integer.parseInt(read.readLine());
                        product.setAvailNum(avaNum1);
                        break;
                    } catch (Exception e) {
                        System.out.print("Availability Number : ");
                    }
                }
                System.out.print("Description : ");
                String desc1 = read.readLine();
                product.setDescription(desc1);
                System.out.print("Supplier Name :");
                String supName1 = read.readLine();
                product.getSupplier().setSupplierName(supName1);
                System.out.print("Supplier Address : ");
                String supAdd1 = read.readLine();
                product.getSupplier().setSupplierAdd(supAdd1);
                System.out.print("Supplier Contact Number : ");
                long supNum1;
                while (true) {
                    try {
                        supNum1 = Long.parseLong(read.readLine());
                        product.getSupplier().setSupplierNum(supNum1);
                        break;
                    } catch (Exception e) {
                        System.out.print("Supplier Contact Number : ");
                    }
                }
                System.out.println("");
                System.out.println("View Product");
                System.out.println("Name of the Product : " + product.getName());
                System.out.println("Price of the Product : " + product.getPrice());
                System.out.println("Availability Number : " + product.getAvailNum());
                System.out.println("Description : " + product.getDescription());
                System.out.println("Supplier Name :" + product.getSupplier().getSupplierName());
                System.out.println("Supplier Address : " + product.getSupplier().getSupplierAdd());
                System.out.println("Supplier Number : " + product.getSupplier().getSupplierNum());

            } else if (result.equalsIgnoreCase("N")) {
                System.out.println("");
                System.out.println("No update has been done, Showing the original Data");
                System.out.println("View Product");
                System.out.println("Name of the Product : " + product.getName());
                System.out.println("Price of the Product : " + product.getPrice());
                System.out.println("Availability Number : " + product.getAvailNum());
                System.out.println("Description : " + product.getDescription());
                System.out.println("Supplier Name :" + product.getSupplier().getSupplierName());
                System.out.println("Supplier Address : " + product.getSupplier().getSupplierAdd());
                System.out.println("Supplier Number : " + product.getSupplier().getSupplierNum());
            }
        } catch (IOException e) {
            Logger.getLogger(CLClass.class.getName()).log(Level.OFF, null, e);
        }
    }
}
