/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Date;

/**
 *
 * @author rajsharavan
 */
public class Fleet {

    private String fleetName;
    private Date date;
    private String flightNo;
    private String travelTime;
    private Date fromTime;
    private Date toTime;
    private int seat;
    private String deptPeriod;
    private String fromLoc;
    private String toLoc;
    private String[][] seatReserve;
    private Customer customer;
    private CustomerDirectory customerDirectory;

    public Fleet() {
        customer = new Customer();
        customerDirectory = new CustomerDirectory();
    }

    public String[][] getSeatReserve() {
        return seatReserve;
    }

    public void setSeatreserve(Fleet fleet) {
        System.out.println(fleet.getSeat());
        int row = (fleet.getSeat()) / 6;
        int column = 6;
        String seatreserv[][] = new String[row][column];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                seatreserv[i][j] = "EMPTY_SEAT";
            }
        }
        this.seatReserve = seatreserv;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public String getFromLoc() {
        return fromLoc;
    }

    public void setFromLoc(String fromLoc) {
        this.fromLoc = fromLoc;
    }

    public String getToLoc() {
        return toLoc;
    }

    public void setToLoc(String toLoc) {
        this.toLoc = toLoc;
    }

    public String getDeptPeriod() {
        return deptPeriod;
    }

    public void setDeptPeriod(String deptPeriod) {
        this.deptPeriod = deptPeriod;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public String getFleetName() {
        return fleetName;
    }

    public void setFleetName(String fleetName) {
        this.fleetName = fleetName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(String travelTime) {
        this.travelTime = travelTime;
    }

    public Date getFromTime() {
        return fromTime;
    }

    public void setFromTime(Date fromTime) {
        this.fromTime = fromTime;
    }

    public Date getToTime() {
        return toTime;
    }

    public void setToTime(Date toTime) {
        this.toTime = toTime;
    }

    public String toString() {
        return fleetName;
    }
}
