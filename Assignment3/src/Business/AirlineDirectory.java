/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author rajsharavan
 */
public class AirlineDirectory {

    private ArrayList<Airline> airlineDirectory;

    public AirlineDirectory() {
        airlineDirectory = new ArrayList<Airline>();
    }

    public ArrayList<Airline> getAirlineDirectory() {
        return airlineDirectory;
    }

    public void setAirlineDirectory(ArrayList<Airline> airlineDirectory) {
        this.airlineDirectory = airlineDirectory;
    }

    public Airline addAirline() {
        Airline airline = new Airline();
        airlineDirectory.add(airline);
        return airline;
    }

    public void deleteAirline(Airline airline) {
        airlineDirectory.remove(airline);
    }
}
