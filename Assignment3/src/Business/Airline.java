/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author rajsharavan
 */
public class Airline {

    private String name;
    private FleetDirectory fleetDir;
    private AirlineDirectory airlineDir;
    private Fleet fleet;

    public Airline() {
        fleetDir = new FleetDirectory();
        airlineDir = new AirlineDirectory();
        fleet = new Fleet();
    }

    public Fleet getFleet() {
        return fleet;
    }

    public void setFleet(Fleet fleet) {
        this.fleet = fleet;
    }

    public AirlineDirectory getAirlineDir() {
        return airlineDir;
    }

    public void setAirlineDir(AirlineDirectory airlineDir) {
        this.airlineDir = airlineDir;
    }

    public FleetDirectory getFleetDir() {
        return fleetDir;
    }

    public void setFleetDir(FleetDirectory fleetDir) {
        this.fleetDir = fleetDir;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
