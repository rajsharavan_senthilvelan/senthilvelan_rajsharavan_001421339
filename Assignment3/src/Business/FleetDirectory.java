/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author rajsharavan
 */
public class FleetDirectory {

    private ArrayList<Fleet> fleetDirectory;

    public FleetDirectory() {
        fleetDirectory = new ArrayList<Fleet>();
    }

    public ArrayList<Fleet> getFleetDirectory() {
        return fleetDirectory;
    }

    public void setFleetDirectory(ArrayList<Fleet> fleetDirectory) {
        this.fleetDirectory = fleetDirectory;
    }

    public Fleet addFleet() {
        Fleet fleet = new Fleet();
        fleetDirectory.add(fleet);
        return fleet;
    }

    public void deleteFleet(Fleet airline) {
        fleetDirectory.remove(airline);
    }

    public Fleet searchFleet(String name) {
        for (Fleet fleet : this.fleetDirectory) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("mm-dd-yyyy");
//            if (fleet.getFleetName().equalsIgnoreCase(name) || fleet.getFlightNo().equalsIgnoreCase(name) || fleet.getFromLoc().equalsIgnoreCase(name)
//                    || fleet.getToLoc().equalsIgnoreCase(name)
//                    || (dateFormat.format(fleet.getDate()).compareTo(name)) == 0) {
//                return fleet;
//            }
            if (fleet.getFleetName().equalsIgnoreCase(name) || (dateFormat.format(fleet.getDate()).compareTo(name)) == 0 || fleet.getFlightNo().equalsIgnoreCase(name)) {
                return fleet;
            }
        }
        return null;
    }
}
