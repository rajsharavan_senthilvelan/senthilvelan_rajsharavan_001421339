/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rajsharavan
 */
public class VitalSignHistory {

    private ArrayList<VitalSign> vitalHistory;

    public VitalSignHistory() {
        vitalHistory = new ArrayList<VitalSign>();
    }

    public ArrayList<VitalSign> getVitalHistory() {
        return vitalHistory;
    }

    public void setVitalHistory(ArrayList<VitalSign> vitalHistory) {
        this.vitalHistory = vitalHistory;
    }

    public VitalSign create() {
        VitalSign vitalSign = new VitalSign();
        vitalHistory.add(vitalSign);
        return vitalSign;
    }

    public void deleteRow(VitalSign vs) {
        vitalHistory.remove(vs);
    }

    public ArrayList<VitalSign> abnormalData(double max, double min) {
        ArrayList<VitalSign> abnorm = new ArrayList<>();
        for (VitalSign vs : vitalHistory) {
            //System.out.println(vs.getTemperature());
            if (vs.getTemperature() < max && vs.getTemperature() > min) {
                //System.out.println(vs.getTemperature() + "For");
                abnorm.add(vs);
            }
        }
        return abnorm;
    }
}
