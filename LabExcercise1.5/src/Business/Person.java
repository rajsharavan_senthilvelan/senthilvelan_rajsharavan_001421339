/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author rajsharavan
 */
public class Person {

    public String firstName;
    private String lastName;
    private String dob;
    private String phoneNumber;
    public Address localAdd;
    public Address workAdd;
    public Address permanentAdd;

    public Address getWorkAdd() {
        return workAdd;
    }

    public void setWorkAdd(Address workAdd) {
        this.workAdd = workAdd;
    }

    public Address getPermanentAdd() {
        return permanentAdd;
    }

    public void setPermanentAdd(Address permanentAdd) {
        this.permanentAdd = permanentAdd;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Address getLocalAdd() {
        return localAdd;
    }

    public void setLocalAdd(Address localAdd) {
        this.localAdd = localAdd;
    }

}
