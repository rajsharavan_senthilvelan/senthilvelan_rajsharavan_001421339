/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author rajsharavans
 */
public class DemoInfo {
    private String fname;
    private String lname;
    private String number;
    private String dob;
    private String age;
    private String height;
    private String weight;
    private String ssn;
    private AddressInfo addInfo;
    private SavingAccount savingAcc;
    private CheckingAccount checkingAcc;
    private DriverInfo driverInfo;
    private MedicalInfo medInfo;
public DemoInfo(){
    addInfo = new AddressInfo();
    savingAcc = new SavingAccount();
    checkingAcc = new CheckingAccount();
    driverInfo = new DriverInfo();
    medInfo = new MedicalInfo();
}
    public AddressInfo getAddInfo() {
        return addInfo;
    }

    public void setAddInfo(AddressInfo addInfo) {
        this.addInfo = addInfo;
    }

    public SavingAccount getSavingAcc() {
        return savingAcc;
    }

    public void setSavingAcc(SavingAccount savingAcc) {
        this.savingAcc = savingAcc;
    }

    public CheckingAccount getCheckingAcc() {
        return checkingAcc;
    }

    public void setCheckingAcc(CheckingAccount checkingAcc) {
        this.checkingAcc = checkingAcc;
    }

    public DriverInfo getDriverInfo() {
        return driverInfo;
    }

    public void setDriverInfo(DriverInfo driverInfo) {
        this.driverInfo = driverInfo;
    }

    public MedicalInfo getMedInfo() {
        return medInfo;
    }

    public void setMedInfo(MedicalInfo medInfo) {
        this.medInfo = medInfo;
    }

    
    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }
          
}
