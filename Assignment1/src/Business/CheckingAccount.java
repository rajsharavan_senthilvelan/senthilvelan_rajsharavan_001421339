/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author rajsharavans
 */
public class CheckingAccount {
    private String BankName2;
    private String RoutingNum2;
    private String AccNum2;
    private String AccBal2;

    public String getBankName2() {
        return BankName2;
    }

    public void setBankName2(String BankName2) {
        this.BankName2 = BankName2;
    }

    public String getRoutingNum2() {
        return RoutingNum2;
    }

    public void setRoutingNum2(String RoutingNum2) {
        this.RoutingNum2 = RoutingNum2;
    }

    public String getAccNum2() {
        return AccNum2;
    }

    public void setAccNum2(String AccNum2) {
        this.AccNum2 = AccNum2;
    }

    public String getAccBal2() {
        return AccBal2;
    }

    public void setAccBal2(String AccBal2) {
        this.AccBal2 = AccBal2;
    }
    
}
