/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author rajsharavans
 */
public class SavingAccount {
    private String BankName1;
    private String RoutingNum1;
    private String AccNum1;
    private String AccBal1;

    public String getBankName1() {
        return BankName1;
    }

    public void setBankName1(String BankName1) {
        this.BankName1 = BankName1;
    }

    public String getRoutingNum1() {
        return RoutingNum1;
    }

    public void setRoutingNum1(String RoutingNum1) {
        this.RoutingNum1 = RoutingNum1;
    }

    public String getAccNum1() {
        return AccNum1;
    }

    public void setAccNum1r(String AccNum1) {
        this.AccNum1 = AccNum1;
    }

    public String getAccBal1() {
        return AccBal1;
    }

    public void setAccBal1(String AccBal1) {
        this.AccBal1 = AccBal1;
    }
   
}
